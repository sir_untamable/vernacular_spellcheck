# Generated by Django 2.0 on 2017-12-19 05:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spell_check', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dictionary',
            name='score',
            field=models.IntegerField(db_index=True),
        ),
        migrations.AlterField(
            model_name='dictionary',
            name='word',
            field=models.CharField(db_index=True, max_length=255),
        ),
    ]
