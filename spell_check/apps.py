from django.apps import AppConfig


class SpellCheckConfig(AppConfig):
    name = 'spell_check'
