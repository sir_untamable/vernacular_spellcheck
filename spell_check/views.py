from rest_framework.views import APIView
from rest_framework.response import Response
from .spell_check import auto_correct_words


class SpellCheck(APIView):

    def get(self,request,format = None):
        words = request.query_params.getlist("words")
        corrected_words = auto_correct_words(words)
        response = dict(zip(words,corrected_words))
        return Response(response)