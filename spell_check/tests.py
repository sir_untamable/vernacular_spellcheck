from django.test import TestCase
from spell_check.models import Dictionary
from spell_check.spell_check import auto_correct_word,auto_correct_words

# Create your tests here.

class DictionaryTestCase(TestCase):

    def setUp(self):
        Dictionary.objects.create(word='the',score = 2210)
        Dictionary.objects.create(word='king',score = 10)


    def test_high_scores(self):
        check_0 = Dictionary.objects.get_best_matching_word(['the'])
        check_1 = Dictionary.objects.get_best_matching_word(['the','king'])
        check_2 = Dictionary.objects.get_matching_words(['the','king'])

        self.assertEqual(check_0.word,'the')
        self.assertEqual(check_1.word,'the')
        self.assertEqual(check_2[0].word,'the')
        self.assertEqual(check_2[1].word,'king')

class CheckCorrectWord(TestCase):

    def setUp(self):
        Dictionary.objects.create(word = 'the', score = 2210)

    def test_correct_word(self):
        check_0 = auto_correct_word('teh')
        check_1 = auto_correct_word('eth')
        check_2 = auto_correct_words(['teh','eth'])
        self.assertEqual(check_0, 'the' )
        self.assertEqual(check_1, 'the' )
        self.assertEqual(check_2, ['the','the'])

