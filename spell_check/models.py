from django.db import models

# Create your models here.

class DictionaryManager(models.Manager):

    def get_matching_words(self,words):
        return self.filter(word__in = words).order_by('-score')

    def get_best_matching_word(self, words):
        try:
            matching_word = self.get_matching_words(words)[0]
            return matching_word
        except IndexError:
            return None
        
        return {'word':'tree','score':123}


class Dictionary(models.Model):

    word = models.CharField(max_length = 255, db_index = True, unique = True)
    score = models.IntegerField(db_index = True)
    # manager
    objects = DictionaryManager()

    def __str__(self):
        return self.word