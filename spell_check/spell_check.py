from .models import Dictionary
from django.core.cache import cache


_ALPHABET = 'abcdefghijklmnopqrstuvwxyz'


def one_edit_away(word):
    splits = [(word[:i], word[i:]) for i in range(len(word) + 1)]
    deletes = [a + b[1:] for a, b in splits if b]
    transposes = [a + b[1] + b[0] + b[2:] for a, b in splits if len(b) > 1]
    replaces = [a + c + b[1:] for a, b in splits for c in _ALPHABET if b]
    inserts = [a + c + b for a, b in splits for c in _ALPHABET]
    return set(deletes + transposes + replaces + inserts)


def compute_correct_word(word):
    one_edit_words = one_edit_away(word)
    corrected_word = Dictionary.objects.get_best_matching_word(one_edit_words)
    
    for one_edit_word in one_edit_words:

        two_edit_words = one_edit_away(one_edit_word)
        corrected_word_temp = Dictionary.objects.get_best_matching_word(two_edit_words)

        if corrected_word and corrected_word_temp:
            corrected_word = max(corrected_word,corrected_word_temp,key=lambda word: word.score)
        elif corrected_word_temp:
            corrected_word = corrected_word_temp
    
    if corrected_word:
        return corrected_word.word
    


def auto_correct_word(word):
    correct_word = cache.get(word) or compute_correct_word(word)
    if correct_word:
        cache.set(word,correct_word)
        return correct_word


def auto_correct_words(words):
    result = []
    for word in words:
        result.append(auto_correct_word(word))
    return result