import csv
from spell_check.models import Dictionary


def import_csv(file_name):

    with open(file_name, newline='') as csvfile:
        csv_reader =  csv.reader(csvfile)
        
        for row in csv_reader:
            Dictionary.objects.create(word = row[0],score = row[1])
