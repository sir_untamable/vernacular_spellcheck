Django==2.0
djangorestframework==3.7.3
gunicorn==19.7.1
Markdown==2.6.10
psycopg2==2.7.3.2
pytz==2017.3
whitenoise==3.3.1
